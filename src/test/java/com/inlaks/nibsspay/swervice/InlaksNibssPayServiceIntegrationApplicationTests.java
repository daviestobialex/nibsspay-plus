package com.inlaks.nibsspay.swervice;

import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRequestCommand;
import com.inlaks.nibsspay.service.models.nibsspay.response.NibssResponse;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
//@SpringBootTest
public class InlaksNibssPayServiceIntegrationApplicationTests {

//    @Test
//    public void contextLoads() {
//        NibssResponse createNewScheduleResponse = new NibssResponse();
//        createNewScheduleResponse = this.convertStringToObject("07", createNewScheduleResponse);
//        log.info("================== " + createNewScheduleResponse.toString() + "=======");
//    }

//    @Test
//    public void contextLoads2() {
//        NibssResponse createNewScheduleResponse = this.convertStringToObject2("07", NibssResponse.class);
//        log.info("================== " + createNewScheduleResponse.toString() + "=======");
//    }

//    @Test
//    public void contextLoads3() {
//        NibssResponse createNewScheduleResponse = this.convertStringToObject2("{\"content\" : \"07\"}", NibssResponse.class);
//        log.info("================== contextLoads3 : " + createNewScheduleResponse.toString() + "=======");
//    }

    @Test
    public void convert() {
        PaymentRequestCommand payment = new PaymentRequestCommand();
        payment.setDebitDescription("1222");
        payment.setScheduleId("jknkjn");
        payment.setClientId("jjhbjb");
        payment.setDebitAccountNumber("jkbjbhj");
        payment.setDebitBankCode("090");
        String scord = this.convertObjectToXmlString(payment);
        log.info("================== " + scord + "=======");
    }

    public <T> T convertStringToObject(String StringObject, T t) {

//        File xmlFile = new File(StringObject);
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(t.getClass());
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (T) jaxbUnmarshaller.unmarshal(IOUtils.toInputStream(StringObject, StandardCharsets.UTF_8));
        } catch (JAXBException e) {
//            e.printStackTrace();
            try {
                Field f = t.getClass().getDeclaredField("content");
                f.setAccessible(true);
                f.set(t, StringObject);
                return t;
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                log.info(ex.getMessage());
            }
        }
        return null;
    }

    public <T extends Object> T convertStringToObject2(String StringObject, Class<T> t) {

//        File xmlFile = new File(StringObject);
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(t.getClass());
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (T) jaxbUnmarshaller.unmarshal(IOUtils.toInputStream(StringObject, StandardCharsets.UTF_8));
        } catch (JAXBException e) {
            e.printStackTrace();
//            try {
//                T newInstance = t.newInstance();
//                Field f = t.getDeclaredField("content");
//                f.setAccessible(true);
//                f.set(newInstance, StringObject);
//                return newInstance;
//            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
//                log.info(ex.getMessage());
//            } catch (InstantiationException ex) {
//                Logger.getLogger(InlaksNibssPayServiceIntegrationApplicationTests.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
        return null;
    }

    public <T> String convertObjectToXmlString(T t) {
        try {

            //Create JAXB Context and Create Marshaller
            Marshaller jaxbMarshaller = JAXBContext.newInstance(t.getClass()).createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(t, sw);

//            log.info(sw.toString());
            //Verify XML Content
            return sw.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

}
