package com.inlaks.nibsspay.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InlaksNibssPayServiceIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(InlaksNibssPayServiceIntegrationApplication.class, args);
	}

}
