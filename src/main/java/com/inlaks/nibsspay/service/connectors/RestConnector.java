/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.connectors;

/**
 *
 * @author ailori
 */
public interface RestConnector<I, O> {

    public O performPostREST(I request, O output, String subUrl);
    
    public O performFull(I request, O output, String subUrl);
}
