package com.inlaks.nibsspay.service.connectors;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Data
@Slf4j
@Component
public class RestConnectorCall<I, O> implements RestConnector<I, O> {

    @Autowired
    protected RestTemplate restTemplate;

    protected String baseURL;

    private String subURL;

    public RestConnectorCall() {
        baseURL = null;
        subURL = null;
    }

    public RestConnectorCall(String dataServiceUrl) {
        baseURL = dataServiceUrl;
        subURL = null;
    }

    @Override
    public O performPostREST(I request, O output, String subUrl) {

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.TEXT_XML);

        HttpEntity<I> httpRequestEntity = new HttpEntity<>(request, headers);

        final val responseEntity = restTemplate.exchange(this.baseURL.concat(subUrl), HttpMethod.POST, httpRequestEntity, output.getClass());

        Logger.getLogger(RestConnectorCall.class.getName()).log(Level.INFO, "Url :::: {0}", baseURL.concat(subUrl));
        Logger.getLogger(RestConnectorCall.class.getName()).log(Level.INFO, "Request :::: {0} ", request);
        Logger.getLogger(RestConnectorCall.class.getName()).log(Level.INFO, "Status Code :::: {0} ", responseEntity.getStatusCode());
        Logger.getLogger(RestConnectorCall.class.getName()).log(Level.INFO, "Body :::: {0} ", responseEntity.getBody());
        return (O) responseEntity.getBody();
    }

    @Override
    public O performFull(I request, O output, String fullUrl) {

        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.TEXT_XML);
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<I> httpRequestEntity = new HttpEntity<>(request, headers);

        final val responseEntity = restTemplate.exchange(fullUrl, HttpMethod.POST, httpRequestEntity, output.getClass());

        Logger.getLogger(RestConnectorCall.class.getName()).log(Level.INFO, "Url :::: {0}", baseURL.concat(fullUrl));
        Logger.getLogger(RestConnectorCall.class.getName()).log(Level.INFO, "Request :::: {0} ", request);
        Logger.getLogger(RestConnectorCall.class.getName()).log(Level.INFO, "Status Code :::: {0} ", responseEntity.getStatusCode());
        Logger.getLogger(RestConnectorCall.class.getName()).log(Level.INFO, "Body :::: {0} ", responseEntity.getBody());
        return (O) responseEntity.getBody();
    }

}
