/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.facade;

import com.google.gson.Gson;
import com.inlaks.nibsspay.service.connectors.RestConnector;
import com.inlaks.nibsspay.service.constants.ProjectConfig;
import com.inlaks.nibsspay.service.constants.ProjectConstants;
import static com.inlaks.nibsspay.service.constants.ProjectConstants.NIBBS_PAY_CREATE_SCHEDULE_URL;
import static com.inlaks.nibsspay.service.constants.ProjectConstants.NIBBS_PAY_EXECUTE_SCHEDULE_URL;
import static com.inlaks.nibsspay.service.constants.ProjectConstants.NIBBS_PAY_UPDATE_SCHEDULE_URL;
import com.inlaks.nibsspay.service.entity.PaymentTransactionRecord;
import com.inlaks.nibsspay.service.entity.TransactionDetailsRecord;
import com.inlaks.nibsspay.service.mapper.PaymentMapper;
import com.inlaks.nibsspay.service.models.enums.NibssResposeCode;
import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import com.inlaks.nibsspay.service.models.inlaks.request.BulkPaymentRequest;
import com.inlaks.nibsspay.service.models.inlaks.request.PaymentRequest;
import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRequestCommand;
import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRequestProcess;
import com.inlaks.nibsspay.service.models.nibsspay.request.ProcessPaymentRequest;
import com.inlaks.nibsspay.service.models.nibsspay.response.NibssResponse;
import com.inlaks.nibsspay.service.models.nibsspay.response.PaymentCountResponse;
import com.inlaks.nibsspay.service.repository.impl.PaymentTransactionRecordService;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.inlaks.dto.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentCountRequest;

/**
 *
 * @author ailori
 */
@Slf4j
@Component
@EnableAsync
public class PaymentFacade2 extends ProjectConfig implements ProjectConstants {

    @Autowired
    private PaymentTransactionRecordService paymentTransactionRecordService;

    @Autowired
    private PaymentMapper paymentMapper;

    @Autowired
    private RestConnector<String, String> restConnector;

    public PaymentCountResponse count(String requestRef, String pageNumber) {

        PaymentTransactionRecord findByRequestId = paymentTransactionRecordService.findByRequestId(requestRef);

        PaymentCountRequest request = new PaymentCountRequest();

        request.setClientId(getClientId());
        request.setPageNumber(pageNumber);
        request.setScheduleId(findByRequestId.getBatchId());

        String requestString = paymentMapper.convertObjectToXmlString(request);
        String connectorResponse = restConnector.performPostREST(requestString, new String(), NIBBS_PAY_COUNT_URL);

//        PaymentCountResponse response = paymentMapper.convertStringToObject(connectorResponse, PaymentCountResponse.class);
        Gson g = new Gson();
        PaymentCountResponse response = g.fromJson(connectorResponse, PaymentCountResponse.class);
//        log.info("=============== " + response.toString() + "  ==========");
        return response;
    }

    // DATE FORMAT SHOULD BE IN THIS FORMAT yyyyMMdd
    public BaseResponse createSchedule(BulkPaymentRequest request) {

        BaseResponse response = new BaseResponse();

        // convert to nibss request
        PaymentRequestCommand paymentRequestCommand = paymentMapper.mapRequestToPaymentSchedule.apply(request);

        // Convert record object and save
        PaymentTransactionRecord record = paymentMapper.mapRequestToPaymentTransactionRecord.apply(paymentRequestCommand);
        record.setPaymentStatus(PaymentStatus.CREATED);
        record.setRequestId(request.getRequestId());
        record.setScheduledDate(request.getScheduledDate());

        // send REST request to nibss
        String string = paymentMapper.convertObjectToXmlString(paymentRequestCommand);
        record.setCreateRequestPayload(string);

        string = restConnector.performPostREST(string, new String(), NIBBS_PAY_CREATE_SCHEDULE_URL);
        record.setCreateResponsePayload(string);

        // save payment records
        this.save(request, record);

        NibssResponse createNewScheduleResponse = paymentMapper.convertStringToObject(string, NibssResponse.class);

        if (createNewScheduleResponse.getContent().equals(NibssResposeCode.RESPONSE_16.getResponseCode())) {
            createNewScheduleResponse = this.updateNibssRecords(record.getRecordDetails());
            // proccess immediately for instant, if failed use tsq to get full details of transaction
            if (request.isInstant()) {
                this.proccess(record.getRecordDetails());
            }
        }

        response.setResponseCode(NibssResposeCode.getInlaksAgencyResponseMapFromCode(createNewScheduleResponse != null ? createNewScheduleResponse.getContent() : null).getResponseCode());
        response.setResponseDescription(NibssResposeCode.getInlaksAgencyResponseMapFromCode(createNewScheduleResponse != null ? createNewScheduleResponse.getContent() : null).getResponseDescription());

        response.setTransactionRef(record.getBatchId());

        return response;
    }

    private void save(BulkPaymentRequest request, PaymentTransactionRecord record) {
        List<TransactionDetailsRecord> details = new ArrayList<>();
        int serialNo = 0;
        for (PaymentRequest req : request.getRequests()) {
            TransactionDetailsRecord detail = paymentMapper.mapRequestToTransactionDetails.apply(req);
            detail.setPaymentTransactionRecord(record);
            detail.setDebitDescription(request.getDebitAccountName());
            detail.setDebitAccountNumber(request.getDebitAccountNumber());
            detail.setDebitBankCode(request.getDebitBankCode());
            detail.setSerialNo(Long.toString(serialNo++));
            details.add(detail);
        }
        record.setRecordDetails(details);
        paymentTransactionRecordService.save(record);
    }

    @Async
    public void proccess(List<TransactionDetailsRecord> record) {

        ProcessPaymentRequest apply = paymentMapper.mapRequestToProcessPaymentRequest.apply(record);

        String requestString = paymentMapper.convertObjectToXmlString(apply);
        String connectorResponse = restConnector.performPostREST(requestString, new String(), NIBBS_PAY_EXECUTE_SCHEDULE_URL);

        // set transaction payload for auditing
        record.get(0).getPaymentTransactionRecord().setProcessRequestPayload(requestString);
        record.get(0).getPaymentTransactionRecord().setProcessResponsePayload(connectorResponse);

        NibssResponse createNewScheduleResponse = paymentMapper.convertStringToObject(connectorResponse, NibssResponse.class);

        if (createNewScheduleResponse.getContent().equals(NibssResposeCode.RESPONSE_16.getResponseCode())) {
            record.get(0).getPaymentTransactionRecord().setPaymentStatus(PaymentStatus.EXECUTED);
        } else {
            record.get(0).getPaymentTransactionRecord().setPaymentStatus(PaymentStatus.FAILED_THIRD_LEVEL);
        }

        // update record if third request failed
        paymentTransactionRecordService.save(record.get(0).getPaymentTransactionRecord());
    }

    private NibssResponse updateNibssRecords(List<TransactionDetailsRecord> record) {

        PaymentRequestProcess apply = paymentMapper.mapRequestToPaymentRequestProcess.apply(record);

        String requestString = paymentMapper.convertObjectToXmlString(apply);
        String connectorResponse = restConnector.performPostREST(requestString, new String(), NIBBS_PAY_UPDATE_SCHEDULE_URL);

        // set transaction payload for auditing
        record.get(0).getPaymentTransactionRecord().setUpdateRequestPayload(requestString);
        record.get(0).getPaymentTransactionRecord().setUpdateResponsePayload(connectorResponse);

        NibssResponse createNewScheduleResponse = paymentMapper.convertStringToObject(connectorResponse, NibssResponse.class);

        if (createNewScheduleResponse.getContent().equals(NibssResposeCode.RESPONSE_16.getResponseCode())) {
            record.get(0).getPaymentTransactionRecord().setPaymentStatus(PaymentStatus.UPDATED);
        } else {
            record.get(0).getPaymentTransactionRecord().setPaymentStatus(PaymentStatus.FAILED_SECOND_LEVEL);
            createNewScheduleResponse = null;
        }
        // update record
        paymentTransactionRecordService.save(record.get(0).getPaymentTransactionRecord());
        return createNewScheduleResponse;
    }
}
