///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.inlaks.nibsspay.service.facade;
//
//import com.inlaks.nibsspay.service.connectors.RestConnector;
//import com.inlaks.nibsspay.service.constants.ProjectConfig;
//import com.inlaks.nibsspay.service.constants.ProjectConstants;
//import com.inlaks.nibsspay.service.entity.PaymentTransactionRecord;
//import com.inlaks.nibsspay.service.entity.TransactionDetailsRecord;
//import com.inlaks.nibsspay.service.mapper.PaymentMapper;
//import com.inlaks.nibsspay.service.models.AuditTransactionPayloadInfo;
//import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
//import com.inlaks.nibsspay.service.models.inlaks.request.BulkPaymentRequest;
//import com.inlaks.nibsspay.service.models.inlaks.request.PaymentRequest;
//import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentCountRequest;
//import com.inlaks.nibsspay.service.models.nibsspay.response.PaymentCountResponse;
//import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRequestCommand;
//import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRequestProcess;
//import com.inlaks.nibsspay.service.models.nibsspay.request.ProcessPaymentRequest;
//import com.inlaks.nibsspay.service.models.nibsspay.response.NibssResponse;
//import com.inlaks.nibsspay.service.repository.impl.PaymentTransactionRecordService;
//import com.inlaks.nibsspay.service.repository.impl.TransactionDetailsRecordService;
//import java.util.ArrayList;
//import java.util.List;
//import lombok.extern.slf4j.Slf4j;
//import org.inlaks.dto.BaseResponse;
//import org.inlaks.responsecode.InlaksResponseCodeEnum;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// *
// * @author ailori
// */
//@Slf4j
//@Component
//public class PaymentFacade extends ProjectConfig implements ProjectConstants {
//
//    @Autowired
//    private PaymentTransactionRecordService paymentTransactionRecordService;
//
//    @Autowired
//    private PaymentMapper paymentMapper;
//
//    @Autowired
//    private RestConnector<String, String> restConnector;
//
//    @Autowired
//    private AuditTransactionPayloadInfo auditTransactionPayloadInfo;
//
//    public PaymentCountResponse count(String requestRef, String pageNumber) {
//        PaymentCountResponse response = new PaymentCountResponse();
//
//        PaymentTransactionRecord findByRequestId = paymentTransactionRecordService.findByRequestId(requestRef);
//
//        PaymentCountRequest request = new PaymentCountRequest();
//
//        request.setClientId(getClientId());
//        request.setPageNumber(pageNumber);
//        request.setScheduleId(findByRequestId.getBatchId());
//
//        String requestString = paymentMapper.convertObjectToXmlString(request);
//        String connectorResponse = restConnector.performPostREST(requestString, new String(), NIBBS_PAY_COUNT_URL);
//
//        response = (PaymentCountResponse) paymentMapper.convertStringToObject(connectorResponse, response);
//
//        return response;
//    }
//
//    /**
//     * Payment Processing function
//     *
//     * @param request
//     * @return
//     */
//    public BaseResponse processPayment(BulkPaymentRequest request) {
//        log.info("==== STARTING FIRST LEVEL ==== ");
//        BaseResponse response = new BaseResponse();
//
//        // convert to nibss request
//        PaymentRequestCommand paymentRequestCommand = paymentMapper.mapRequestToPaymentSchedule.apply(request);
//
//        // Convert record object and save
//        PaymentTransactionRecord record = paymentMapper.mapRequestToPaymentTransactionRecord.apply(paymentRequestCommand);
//        record.setPaymentStatus(PaymentStatus.CREATED);
//        record.setRequestId(request.getRequestId());
//
//        // joining the transaction details 
//        List<TransactionDetailsRecord> details = new ArrayList<>();
//
//        TransactionDetailsRecord detail = paymentMapper.mapRequestToTransactionDetails.apply(request);
//        detail.setPaymentTransactionRecord(record);
//        detail.setSerialNo("1");
//        details.add(detail);
//
//        log.info("===== RECORDS ==" + details.size());
//        record.setRecordDetails(details);
//
//        // push first level request to nibss
//        String requestString = paymentMapper.convertObjectToXmlString(paymentRequestCommand);
//        String connectorResponse = restConnector.performPostREST(requestString, new String(), NIBBS_PAY_CREATE_SCHEDULE_URL);
//
//        // set transaction payload for auditing
//        auditTransactionPayloadInfo.setFirstRequestPayload(requestString);
//        auditTransactionPayloadInfo.setFirstResponsePayload(connectorResponse);
//
//        NibssResponse createNewScheduleResponse = paymentMapper.convertStringToObject(connectorResponse, NibssResponse.class);
//
//        if (createNewScheduleResponse.getContent().equals("16") && beginSecondLevelRequest(request, record)) {
//            response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_00.getResponseCode());
//            response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_00.getResponseDescription());
//        } else {
//
//            record.setPaymentStatus(PaymentStatus.FAILED_FIRST_LEVEL);
//
//            // update record if third request failed
//            paymentTransactionRecordService.save(record);
//
//            response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
//            response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());
//        }
//        return response;
//
//    }
//
//    /**
//     * Starts the second level transaction for nibss update schedule
//     *
//     * @param request
//     * @param record
//     * @return
//     */
//    private boolean beginSecondLevelRequest(PaymentRequest request, PaymentTransactionRecord record) {
//        log.info("==== STARTING SECOND LEVEL ==== scheduled = " + request.isScheduled());
//
//        PaymentRequestProcess apply = paymentMapper.mapRequestToPaymentRequestProcess.apply(request);
//        apply.setScheduleId(record.getBatchId());
//
////        TransactionDetailsRecord detailsRecord = new TransactionDetailsRecord();
////        log.info("==== " + apply.getPaymentRecords().get(0).toString());
////        BeanUtils.copyProperties(apply.getPaymentRecords().get(0), detailsRecord);
////        log.info("==== " + detailsRecord);
////        detailsRecord.setPaymentTransactionRecord(record);
////        record.setRecordDetails(Arrays.asList(detailsRecord));
//        // update record of second level transaction
////        transactionDetailsRecordService.save(detailsRecord);
////        if (request.isScheduled()) {
////            String requestString = "", connectorResponse = "";
////            for (int i = 0; i < 500; i++) {
////                apply.getPaymentRecords().get(0).setSerialNo(Integer.toString(i));
////                requestString = paymentMapper.convertObjectToXmlString(apply);
////                connectorResponse = restConnector.performPostREST(requestString, new String(), NIBBS_PAY_UPDATE_SCHEDULE_URL);
////            }
////            NibssResponse createNewScheduleResponse = new NibssResponse();
////            createNewScheduleResponse = (NibssResponse) paymentMapper.convertStringToObject(connectorResponse, createNewScheduleResponse);
////
////            if (createNewScheduleResponse.getContent().equals("16") && beginThirdLevelRequest(request, record)) {
////                record.setPaymentStatus(PaymentStatus.UPDATED);
////                paymentTransactionRecordService.save(record);
////                return true;
////            }
////
////            record.setPaymentStatus(PaymentStatus.FAILED_SECOND_LEVEL);
////            // update record if third request failed
////            paymentTransactionRecordService.save(record);
////            log.info(" ====== SECOND LEVEL EXIT =====" + createNewScheduleResponse.toString());
////            
////            return beginThirdLevelRequest(request, record);
////        } else {
//        String requestString = paymentMapper.convertObjectToXmlString(apply);
//        String connectorResponse = restConnector.performPostREST(requestString, new String(), NIBBS_PAY_UPDATE_SCHEDULE_URL);
//
//        // set transaction payload for auditing
//        auditTransactionPayloadInfo.setSecondRequestPayload(requestString);
//        auditTransactionPayloadInfo.setSecondResponsePayload(connectorResponse);
//
//        NibssResponse createNewScheduleResponse = new NibssResponse();
//        createNewScheduleResponse = (NibssResponse) paymentMapper.convertStringToObject(connectorResponse, createNewScheduleResponse);
//        if (createNewScheduleResponse.getContent().equals("16") && beginThirdLevelRequest(request, record)) {
//            record.setPaymentStatus(PaymentStatus.UPDATED);
//            paymentTransactionRecordService.save(record);
//            return true;
//        }
//
//        record.setPaymentStatus(PaymentStatus.FAILED_SECOND_LEVEL);
//        // update record if third request failed
//        paymentTransactionRecordService.save(record);
//
//        return false;
////        }
//    }
//
//    /**
//     * Third and final level transaction request to nibss
//     *
//     * @param request
//     * @param record
//     * @return
//     */
//    private boolean beginThirdLevelRequest(PaymentRequest request, PaymentTransactionRecord record) {
//
//        record.setPaymentStatus(PaymentStatus.EXECUTED);
//
//        // update record of second level transaction
//        paymentTransactionRecordService.save(record);
//        ProcessPaymentRequest apply = paymentMapper.mapRequestToProcessPaymentRequest.apply(request);
//        apply.setScheduleId(record.getBatchId());
//        String requestString = paymentMapper.convertObjectToXmlString(apply);
//        String connectorResponse = restConnector.performPostREST(requestString, new String(), NIBBS_PAY_EXECUTE_SCHEDULE_URL);
//
//        // set transaction payload for auditing
//        auditTransactionPayloadInfo.setThirdRequestPayload(requestString);
//        auditTransactionPayloadInfo.setThirdResponsePayload(connectorResponse);
//
//        NibssResponse createNewScheduleResponse = new NibssResponse();
//        createNewScheduleResponse = (NibssResponse) paymentMapper.convertStringToObject(connectorResponse, createNewScheduleResponse);
//
//        if (createNewScheduleResponse.getContent().equals("16")) {
//            return true;
//        }
//
//        record.setPaymentStatus(PaymentStatus.FAILED_THIRD_LEVEL);
//
//        // update record if third request failed
//        paymentTransactionRecordService.save(record);
//
//        return false;
//    }
//}
