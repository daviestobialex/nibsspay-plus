/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.facade;

import com.inlaks.nibsspay.service.entity.PaymentTransactionRecord;
import com.inlaks.nibsspay.service.models.enums.NibssResposeCode;
import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import com.inlaks.nibsspay.service.models.nibsspay.response.PaymentStatusResponse;
import com.inlaks.nibsspay.service.repository.impl.PaymentTransactionRecordService;
import lombok.extern.slf4j.Slf4j;
import org.inlaks.dto.BaseResponse;
import org.inlaks.responsecode.InlaksResponseCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ailori
 */
@Slf4j
@Component
public class CallbackFacade {
    
    @Autowired
    private PaymentTransactionRecordService paymentTransactionRecordService;

    /**
     * This is the method NIBSSPAY PLUS calls on the customer’s web service as
     * soon as payment status is to be shared.
     *
     * @param response
     * @return
     */
    public BaseResponse processCallback(PaymentStatusResponse response) {
        log.info(" ==== " + response.toString() + " =====");
        BaseResponse baseResponse = new BaseResponse();
        
        PaymentTransactionRecord findByBatchId = paymentTransactionRecordService.findByBatchId(response.getScheduleId());
//        NibssResposeCode.getInlaksAgencyResponseMapFromCode(repsonse.getStatus())
        if (findByBatchId != null) {
            findByBatchId.setPaymentStatus(response.getStatus().equals("16") ? PaymentStatus.SUCCSSFUL : PaymentStatus.FAILED_CALL_BACK);

//            if (findByBatchId.getRecordDetails().size() == 1) {
            findByBatchId.getRecordDetails().forEach((detail) -> {
                response.getPaymentRecords().forEach((rec) -> {
                    if (detail.getSerialNo().equals(rec.getSerialNo())) {
                        detail.setPaymentStatus(response.getPaymentRecords().get(0).getStatus().equals(NibssResposeCode.RESPONSE_00.getResponseCode()) ? PaymentStatus.SUCCSSFUL : PaymentStatus.FAILED_CALL_BACK);
                        detail.setResponseCallbackCode(rec.getStatus());
                    }
                });
                
            });
//            } else {
//                baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_98.getResponseCode());
//                baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_98.getResponseDescription());
//            }

            paymentTransactionRecordService.save(findByBatchId);
            
            baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_00.getResponseCode());
            baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_00.getResponseDescription());
        } else {
            baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_38.getResponseCode());
            baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_38.getResponseDescription());
        }
        
        return baseResponse;
    }
}
