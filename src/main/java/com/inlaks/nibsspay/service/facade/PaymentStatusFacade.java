/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.facade;

import com.google.gson.Gson;
import com.inlaks.nibsspay.service.connectors.RestConnector;
import com.inlaks.nibsspay.service.constants.ProjectConfig;
import com.inlaks.nibsspay.service.constants.ProjectConstants;
import com.inlaks.nibsspay.service.mapper.PaymentMapper;
import com.inlaks.nibsspay.service.models.enums.NibssResposeCode;
import com.inlaks.nibsspay.service.models.inlaks.response.BalanceResponse;
import com.inlaks.nibsspay.service.models.inlaks.response.TransactionStatusQueryResponse;
import com.inlaks.nibsspay.service.models.nibsspay.request.BalanceRequest;
import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentStatusRequest;
import com.inlaks.nibsspay.service.models.nibsspay.request.RequeryRequest;
import com.inlaks.nibsspay.service.models.nibsspay.response.PaymentStatusResponse;
import com.inlaks.nibsspay.service.repository.impl.PaymentTransactionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ailori
 */
@Component
public class PaymentStatusFacade extends ProjectConfig implements ProjectConstants {

    @Autowired
    private RestConnector<String, String> restConnector;

    @Autowired
    private PaymentTransactionRecordService paymentTransactionRecordService;

    @Autowired
    private PaymentMapper paymentMapper;

    /**
     * Get Status Query From NIBSS
     *
     * @param requestId
     * @return
     */
    public TransactionStatusQueryResponse startStatusQuery(String requestId) {

        String connectorResponse = restConnector.performPostREST(paymentMapper.convertObjectToXmlString(new PaymentStatusRequest(paymentTransactionRecordService.findByRequestId(requestId).getBatchId(), getClientId())), new String(), NIBBS_PAY_STATUS_SCHEDULE_URL);
        Gson g = new Gson();
        return paymentMapper.mapStatusResponseToTSQ.apply(g.fromJson(connectorResponse, PaymentStatusResponse.class));
    }

    /**
     * Get status Query From Database
     *
     * @param requestId
     * @return
     */
    public TransactionStatusQueryResponse startDbStatusQuery(String requestId) {
        return paymentMapper.mapTransactionRecordToStatusResponse.apply(paymentTransactionRecordService.findByRequestId(requestId));
    }

    /**
     * This method is used to request NIBSSPAY PLUS to re-send the status of
     * concluded transactions in a batch i.e. records that have gone to the bank
     * and are back with status PAID or FAILED
     *
     * @param requestId
     * @return
     */
    public TransactionStatusQueryResponse startReQuery(String requestId) {
        String id = paymentTransactionRecordService.findByRequestId(requestId).getBatchId();
        String connectorResponse = restConnector.performPostREST(paymentMapper.convertObjectToXmlString(new RequeryRequest(id, getClientId())), new String(), NIBBS_PAY_STATUS_REQUERY_URL);
        TransactionStatusQueryResponse response = new TransactionStatusQueryResponse();
        response.setScheduleId(id);
        response.setTransactionRef(requestId);
        response.setResponseCode(NibssResposeCode.getInlaksAgencyResponseMapFromCode(connectorResponse).getResponseCode());
        response.setResponseDescription(NibssResposeCode.getInlaksAgencyResponseMapFromCode(connectorResponse).getResponseDescription());
        return response;
    }

    public BalanceResponse balanceEnq(String bankCode, String accountNumber) {

        String connectorResponse = restConnector.performFull(paymentMapper.convertObjectToXmlString(new BalanceRequest(getClientId(), bankCode, accountNumber)), new String(), getBalanceUrl());

        return paymentMapper.convertStringToObject(connectorResponse, BalanceResponse.class);
    }
}
