/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.entity;

import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.Data;

/**
 *
 * @author ailori
 */
@Data
@Entity
public class TransactionDetailsRecord implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "batch_id", nullable = false)
    private PaymentTransactionRecord paymentTransactionRecord;

    @Column(nullable = false)
    private String debitBankCode;

    @Column(nullable = false)
    private String debitAccountNumber;

    @Column(nullable = false)
    private String debitDescription;

    @Column(nullable = false)
    private String beneficiary;

    @Column(nullable = false)
    private String amount;

    @Column(nullable = false)
    private String accountNumber;

    @Column(nullable = false)
    private String bankCode;

    @Column(nullable = true)
    private String serialNo;

    @Column(nullable = false)
    private PaymentStatus paymentStatus;

    @Column(nullable = true)
    private String responseCallbackCode;

    @Lob
    private String narration;
}
