/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.entity;

import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * This is the parent transaction record of the batch payment processor
 *
 * @author ailori
 */
@Data
@Entity
public class PaymentTransactionRecord implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String requestId;

    @Column(nullable = false, unique = true)
    private String batchId;

    private String clientId;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "paymentTransactionRecord")
    private List<TransactionDetailsRecord> recordDetails;

    @Column(nullable = false)
    private PaymentStatus paymentStatus;

    @Column(nullable = true)
    private String scheduledDate;

    @Lob
    private String createRequestPayload;

    @Lob
    private String createResponsePayload;
    
    @Lob
    private String updateRequestPayload;

    @Lob
    private String updateResponsePayload;
    
    @Lob
    private String processRequestPayload;

    @Lob
    private String processResponsePayload;

    @CreationTimestamp
    private Timestamp dateCreated;

    @Column(insertable = false, updatable = true)
    @UpdateTimestamp
    private Timestamp lastModified;

}
