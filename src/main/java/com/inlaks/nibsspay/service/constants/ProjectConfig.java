/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.constants;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @author ailori
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties
@PropertySource("file:./config/projectConfig.properties")
public class ProjectConfig {

    private String clientId;

    private String baseUrl;

    private String balanceUrl;
}
