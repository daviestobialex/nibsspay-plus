/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.constants;

/**
 *
 * @author ailori
 */
public interface UrlMapping {

    public final String ACCESS_ROOT_MAP = "/v1";

    public final String PAYMENT_URL = "/pay";
    
    public final String PAYMENT_COUNT_URL = "/pay/count/{requestRef}/{pageNumber}";

    public final String STATUS_QUERY_URL = "/tsq/{requestId}";

    public final String STATUS_DB_QUERY_URL = "/db/tsq/{requestId}";

    public final String STATUS_REQUERY_URL = "/requery/{requestId}";
    
    public final String BALANCE_ENQUIRY_URL = "/balance/{bankCode}/{accountNumber}";
    
    public final String CALLBACK_URL = "/callback";
}
