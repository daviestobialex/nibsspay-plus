/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.constants;

/**
 *
 * @author ailori
 */
public interface ProjectConstants {

    public String TXN_REFERENCE_DATE_FORMAT = "yyyyMMddHHmmss";

    public String NIBBS_PAY_CREATE_SCHEDULE_URL = "/new";

    public String NIBBS_PAY_UPDATE_SCHEDULE_URL = "/update";

    public String NIBBS_PAY_EXECUTE_SCHEDULE_URL = "/process";

    public String NIBBS_PAY_STATUS_SCHEDULE_URL = "/status";

    public String NIBBS_PAY_STATUS_REQUERY_URL = "/requery";

    public String NIBBS_PAY_COUNT_URL = "/count";
}
