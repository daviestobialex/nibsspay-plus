/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.scheduler;

import com.inlaks.nibsspay.service.entity.PaymentTransactionRecord;
import com.inlaks.nibsspay.service.facade.PaymentFacade2;
import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import com.inlaks.nibsspay.service.repository.impl.PaymentTransactionRecordService;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 *
 * @author ailori
 */
@EnableScheduling
public class ProccessPayments {

    @Autowired
    private PaymentTransactionRecordService paymentTransactionRecordService;

    @Autowired
    private PaymentFacade2 paymentFacade;

    /**
     * DATE FORMAT SHOULD BE IN THIS FORMAT yyyyMMdd process daily records
     */
    @Scheduled(fixedRate = 86200000)
    public void ProcessPayment() {
        List<PaymentTransactionRecord> findByPaymentStatus = paymentTransactionRecordService.findByPaymentStatus(PaymentStatus.UPDATED);
        if (findByPaymentStatus != null && !findByPaymentStatus.isEmpty()) {
            findByPaymentStatus.stream().forEach(record -> {
                try {
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    Date date = new Date();

                    if (dateFormat.parse(record.getScheduledDate()).compareTo(date) == 0) {
                        paymentFacade.proccess(record.getRecordDetails());
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(ProccessPayments.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }

    }
}
