package com.inlaks.nibsspay.service.config;

import com.inlaks.nibsspay.service.connectors.RestConnector;
import com.inlaks.nibsspay.service.connectors.RestConnectorCall;
import com.inlaks.nibsspay.service.constants.ProjectConfig;
import com.inlaks.nibsspay.service.constants.ProjectConstants;
import com.inlaks.nibsspay.service.models.TransactionReferenceBean;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import java.util.Date;
import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;
import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Configuration
public class BeanConfig extends ProjectConfig implements ProjectConstants {

    @Bean
    @Scope(value = SCOPE_PROTOTYPE, proxyMode = TARGET_CLASS)
    public TransactionReferenceBean transactionReferenceBean() {
        TransactionReferenceBean transactionReferenceBean = new TransactionReferenceBean();
        val builder = new StringBuilder();
        transactionReferenceBean.setTransactionReference(builder.append(DateFormatUtils.format(new Date(), TXN_REFERENCE_DATE_FORMAT))
                .append(RandomStringUtils.randomNumeric(4)).toString());
        return transactionReferenceBean;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Scope(value = SCOPE_PROTOTYPE, proxyMode = TARGET_CLASS)
    public RestConnector restConnector() {
        return new RestConnectorCall(getBaseUrl());
    }

}
