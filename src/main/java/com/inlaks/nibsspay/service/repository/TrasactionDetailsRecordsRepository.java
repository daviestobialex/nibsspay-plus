/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.repository;

import com.inlaks.nibsspay.service.entity.TransactionDetailsRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ailori
 */
@Repository
public interface TrasactionDetailsRecordsRepository extends CrudRepository<TransactionDetailsRecord, Long> {

}
