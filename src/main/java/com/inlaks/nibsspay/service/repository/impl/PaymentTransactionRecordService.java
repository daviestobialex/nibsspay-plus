/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.repository.impl;

import com.inlaks.nibsspay.service.entity.PaymentTransactionRecord;
import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import com.inlaks.nibsspay.service.repository.PaymentTransactionRecordRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ailori
 */
@Service
public class PaymentTransactionRecordService extends DataService<PaymentTransactionRecord> {
    
    @Autowired
    private PaymentTransactionRecordRepository paymentTransactionRecordRepository;
    
    @Override
    public PaymentTransactionRecord save(PaymentTransactionRecord record) {
        return paymentTransactionRecordRepository.save(record);
    }
    
    @Override
    public PaymentTransactionRecord findByRequestId(String requestId) {
        return paymentTransactionRecordRepository.findByRequestId(requestId).orElse(null);
    }
    
    @Override
    public PaymentTransactionRecord findByBatchId(String batchId) {
        return paymentTransactionRecordRepository.findByBatchId(batchId).orElse(null); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<PaymentTransactionRecord> findByPaymentStatus(PaymentStatus paymentStatus) {
        return paymentTransactionRecordRepository.findByPaymentStatus(paymentStatus); //To change body of generated methods, choose Tools | Templates.
    }
}
