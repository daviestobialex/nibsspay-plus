/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.repository;

import com.inlaks.nibsspay.service.entity.PaymentTransactionRecord;
import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author ailori
 */
public interface DataService<I> {

    public I save(I record);

    public I findByRequestId(String requestId);

    public I findByBatchId(String batchId);

    public List<I> findByPaymentStatus(PaymentStatus paymentStatus);
}
