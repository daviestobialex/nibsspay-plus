/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.repository;

import com.inlaks.nibsspay.service.entity.PaymentTransactionRecord;
import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ailori
 */
@Repository
public interface PaymentTransactionRecordRepository extends CrudRepository<PaymentTransactionRecord, Long> {

    public Optional<PaymentTransactionRecord> findByRequestId(String id);

    public Optional<PaymentTransactionRecord> findByBatchId(String id);

    public List<PaymentTransactionRecord> findByPaymentStatus(PaymentStatus paymentStatus);

}
