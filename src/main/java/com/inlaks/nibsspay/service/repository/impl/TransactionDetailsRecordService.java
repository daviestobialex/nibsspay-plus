/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.repository.impl;

import com.inlaks.nibsspay.service.entity.TransactionDetailsRecord;
import com.inlaks.nibsspay.service.repository.TrasactionDetailsRecordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ailori
 */
@Service
public class TransactionDetailsRecordService extends DataService<TransactionDetailsRecord> {

    @Autowired
    private TrasactionDetailsRecordsRepository respository;

    @Override
    public TransactionDetailsRecord save(TransactionDetailsRecord record) {
        return respository.save(record);
    }

}
