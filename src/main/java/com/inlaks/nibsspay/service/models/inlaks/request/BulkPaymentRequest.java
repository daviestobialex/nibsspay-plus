/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.inlaks.request;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.Data;
import org.inlaks.dto.BaseRequest;

/**
 *
 * @author ailori
 */
@ApiModel
@Data
public class BulkPaymentRequest extends BaseRequest {

    private String debitBankCode = "058";

    private String debitAccountName;

    private String debitAccountNumber = "0032758280";

    private String scheduledDate;

    private boolean instant;

    private List<PaymentRequest> requests;
}
