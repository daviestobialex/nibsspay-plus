/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.nibsspay.response;

import lombok.Data;
import lombok.ToString;

/**
 *
 * @author ailori
 */
@Data
@ToString
public class NibssResponse {

    private String content;
}
