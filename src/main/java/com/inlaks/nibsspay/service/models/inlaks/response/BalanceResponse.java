/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.inlaks.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author ailori
 */
@Data
@XmlRootElement(name = "BalanceResponse")
@XmlAccessorType(XmlAccessType.NONE)
public class BalanceResponse {

    @XmlElement(name = "Status")
    private String Status;

    @XmlElement(name = "Amount")
    private String Amount;

    @XmlElement(name = "ClientId")
    private String ClientId;

    @XmlElement(name = "BankCode")
    private String BankCode;

    @XmlElement(name = "Timestamp")
    private String Timestamp;

    @XmlElement(name = "Reason")
    private String Reason;

    @XmlElement(name = "AccountNumber")
    private String AccountNumber;

    @XmlElement(name = "AccountName")
    private String AccountName;
}
