/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.nibsspay.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.inlaks.dto.BaseResponse;

/**
 *
 * @author ailori
 */
@ToString
@Data
public class PaymentCountResponse extends BaseResponse{

    private String Status;

    private String PageCount;

    private String PageNumber;

    @JsonIgnore
    private String ClientId;

    private String ScheduleId;

    private String RecordCount;
}
