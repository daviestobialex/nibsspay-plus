/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.nibsspay.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.ToString;

/**
 *
 * @author ailori
 */
@ToString
@Data
@XmlRootElement(name = "PaymentRecord")
@XmlAccessorType(XmlAccessType.NONE)
public class PaymentRecord {

    @XmlElement(name = "SerialNo")
    private String serialNo;

    @XmlElement(name = "Narration")
    private String narration;

    @XmlElement(name = "Amount")
    private String amount;

    @XmlElement(name = "BankCode")
    private String bankCode;

    @XmlElement(name = "Beneficiary")
    private String beneficiary;

    @XmlElement(name = "AccountNumber")
    private String accountNumber;

    @XmlElement(name = "Status")
    private String status;
}
