package com.inlaks.nibsspay.service.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionReferenceBean {


    private String transactionReference;
}
