/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.nibsspay.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author ailori
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "PaymentStatusRequest")
@XmlAccessorType(XmlAccessType.NONE)
public class PaymentStatusRequest {

    @XmlElement(name = "ScheduleId")
    private String ScheduleId;

    @XmlElement(name = "ClientId")
    private String ClientId;

}
