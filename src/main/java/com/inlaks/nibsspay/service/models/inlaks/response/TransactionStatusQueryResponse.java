/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.inlaks.response;

import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import lombok.Data;
import org.inlaks.dto.BaseResponse;

/**
 *
 * @author ailori
 */
@Data
public class TransactionStatusQueryResponse extends BaseResponse {

    private String scheduleId;

}
