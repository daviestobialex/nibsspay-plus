/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.nibsspay.response;

import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRecord;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author ailori
 */
@Data
@XmlRootElement(name = "PaymentStatusResponse")
@XmlAccessorType(XmlAccessType.NONE)
public class PaymentStatusResponse {

    @XmlElement(name = "PageNumber")
    private String PageNumber;

    @XmlElement(name = "ScheduleId")
    private String ScheduleId;

    @XmlElement(name = "ClientId")
    private String ClientId;

    @XmlElement(name = "Status")
    private String Status;

    @XmlElementWrapper(name = "PaymentRecords")
    @XmlElement(name = "PaymentRecord")
    private List<PaymentRecord> paymentRecords;
}
