/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.inlaks.responsecode.InlaksResponseCodeEnum;

/**
 *
 * @author ailori
 */
@AllArgsConstructor
public enum NibssResposeCode {

    RESPONSE_00("00", "Transaction successful", InlaksResponseCodeEnum.RESPONSE_00),
    RESPONSE_99("99", "SYSTEM ERROR", InlaksResponseCodeEnum.RESPONSE_01),
    RESPONSE_01("01", "DUPLICATE UPLOAD", InlaksResponseCodeEnum.RESPONSE_44),
    RESPONSE_02("02", "MANDATORY FIELD NOT SET", InlaksResponseCodeEnum.RESPONSE_97),
    RESPONSE_03("03", "UNKNOWN CLIENT ID", InlaksResponseCodeEnum.RESPONSE_01),
    RESPONSE_04("04", "UNKNOWN UNIQUE ID", InlaksResponseCodeEnum.RESPONSE_04),
    RESPONSE_05("05", "FORMAT ERROR", InlaksResponseCodeEnum.RESPONSE_25),
    RESPONSE_06("06", "IN PROGRESS", InlaksResponseCodeEnum.RESPONSE_21),
    RESPONSE_07("07", "UNKNOWN DEBIT ACCOUNT", InlaksResponseCodeEnum.RESPONSE_27),
    RESPONSE_08("08", "FAILED NAME VALIDATION", InlaksResponseCodeEnum.RESPONSE_33),
    RESPONSE_09("09", "REJECTED BY APPROVER", InlaksResponseCodeEnum.RESPONSE_36),
    RESPONSE_10("10", "INVALID NUBAN NUMBER", InlaksResponseCodeEnum.RESPONSE_11),
    RESPONSE_11("11", "PAYMENT DISHONOURED BY BANK", InlaksResponseCodeEnum.RESPONSE_36),
    RESPONSE_12("12", "PROCESSING COMPLETED WITH ERROR", InlaksResponseCodeEnum.RESPONSE_01),
    RESPONSE_13("13", "DEBIT ACCOUNT NOT FOUND", InlaksResponseCodeEnum.RESPONSE_33),
    RESPONSE_14("14", "RECORD NOT FOUND", InlaksResponseCodeEnum.RESPONSE_14),
    RESPONSE_15("15", "PAYMENT FAILED", InlaksResponseCodeEnum.RESPONSE_20),
    RESPONSE_16("16", "REQUEST ACCEPTED", InlaksResponseCodeEnum.RESPONSE_21),
    RESPONSE_17("17", "PAYMENT SCHEDULE NOT FOUND", InlaksResponseCodeEnum.RESPONSE_36),
    RESPONSE_18("18", "INVALID SCHEDULE ID", InlaksResponseCodeEnum.RESPONSE_36),
    RESPONSE_19("19", "BENEFICIARY BANK NOT AVAILABLE", InlaksResponseCodeEnum.RESPONSE_72),
    RESPONSE_20("20", "DO NOT HONOR", InlaksResponseCodeEnum.RESPONSE_31),
    RESPONSE_21("21", "DORMANT ACCOUNT", InlaksResponseCodeEnum.RESPONSE_33),
    RESPONSE_22("22", "INVALID BANK CODE", InlaksResponseCodeEnum.RESPONSE_01);

    @Getter
    private String responseCode;

    @Getter
    private String responseDescription;

    @Getter
    private InlaksResponseCodeEnum inlaksResponseCodeEnum;

    public static InlaksResponseCodeEnum getInlaksAgencyResponseMapFromCode(String responseCode) {
        if (responseCode != null) {
            for (NibssResposeCode nibssResposeCode : NibssResposeCode.values()) {
                if (nibssResposeCode.getResponseCode().equalsIgnoreCase(responseCode)) {
                    return nibssResposeCode.getInlaksResponseCodeEnum();
                }
            }
        }
        return InlaksResponseCodeEnum.RESPONSE_17;
    }
}
