/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.inlaks.request;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 *
 * @author ailori
 */
@ApiModel
@Data
public class PaymentRequest {

    private String beneficiaryBankCode;

    private String beneficiaryAccountName;

    private String beneficiaryAccountNumber;

    private int amount;

    private String narration;
}
