/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.models.enums;

/**
 *
 * @author ailori
 */
public enum PaymentStatus {

    CREATED, UPDATED, EXECUTED, SUCCSSFUL, FAILED_CALL_BACK, FAILED_FIRST_LEVEL, FAILED_SECOND_LEVEL, FAILED_THIRD_LEVEL;
}
