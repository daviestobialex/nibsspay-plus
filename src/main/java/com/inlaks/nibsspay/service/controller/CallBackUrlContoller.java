/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.controller;

import static com.inlaks.nibsspay.service.constants.UrlMapping.ACCESS_ROOT_MAP;
import static com.inlaks.nibsspay.service.constants.UrlMapping.CALLBACK_URL;
import com.inlaks.nibsspay.service.facade.CallbackFacade;
import com.inlaks.nibsspay.service.models.nibsspay.response.PaymentStatusResponse;
import io.swagger.annotations.ApiResponse;
import javax.validation.Valid;
import org.inlaks.dto.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ailori
 */
@RestController
@RequestMapping(ACCESS_ROOT_MAP)
public class CallBackUrlContoller {

    @Autowired
    private CallbackFacade callbackFacade;

    @ResponseBody
    @PostMapping(CALLBACK_URL)
    @ApiResponse(code = 200, message = "Success", response = BaseResponse.class)
    public ResponseEntity callBackController(@Valid @RequestBody PaymentStatusResponse requestCallbackResponse) {
        return ResponseEntity.ok().body(callbackFacade.processCallback(requestCallbackResponse));
    }
}
