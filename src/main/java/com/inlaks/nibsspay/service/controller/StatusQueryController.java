/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.controller;

import static com.inlaks.nibsspay.service.constants.UrlMapping.ACCESS_ROOT_MAP;
import static com.inlaks.nibsspay.service.constants.UrlMapping.BALANCE_ENQUIRY_URL;
import static com.inlaks.nibsspay.service.constants.UrlMapping.STATUS_DB_QUERY_URL;
import static com.inlaks.nibsspay.service.constants.UrlMapping.STATUS_QUERY_URL;
import static com.inlaks.nibsspay.service.constants.UrlMapping.STATUS_REQUERY_URL;
import com.inlaks.nibsspay.service.facade.PaymentStatusFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ailori
 */
@RestController
@RequestMapping(ACCESS_ROOT_MAP)
public class StatusQueryController {

    @Autowired
    private PaymentStatusFacade paymentStatusFacade;

    /**
     * This controller goes to nibss to get the status of the transaction
     *
     * @param requestId
     * @return
     */
    @ResponseBody
    @GetMapping(STATUS_QUERY_URL)
    public ResponseEntity statusQuery(@PathVariable String requestId) {
        return ResponseEntity.ok().body(paymentStatusFacade.startStatusQuery(requestId));
    }

    /**
     * This controller goes directly to the database
     *
     * @param requestId
     * @return
     */
    @ResponseBody
    @GetMapping(STATUS_DB_QUERY_URL)
    public ResponseEntity dbStatusQuery(@PathVariable String requestId) {
        return ResponseEntity.ok().body(paymentStatusFacade.startDbStatusQuery(requestId));
    }

    @ResponseBody
    @GetMapping(STATUS_REQUERY_URL)
    public ResponseEntity statusReQuery(@PathVariable String requestId) {
        return ResponseEntity.ok().body(paymentStatusFacade.startReQuery(requestId));
    }

    @ResponseBody
    @GetMapping(value = BALANCE_ENQUIRY_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity balanceEnq(@PathVariable String bankCode, @PathVariable String accountNumber) {
        return ResponseEntity.ok().body(paymentStatusFacade.balanceEnq(bankCode, accountNumber));
    }
}
