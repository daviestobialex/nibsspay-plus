/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.controller;

import static com.inlaks.nibsspay.service.constants.UrlMapping.ACCESS_ROOT_MAP;
import static com.inlaks.nibsspay.service.constants.UrlMapping.PAYMENT_COUNT_URL;
import static com.inlaks.nibsspay.service.constants.UrlMapping.PAYMENT_URL;
import com.inlaks.nibsspay.service.facade.PaymentFacade2;
import com.inlaks.nibsspay.service.models.inlaks.request.BulkPaymentRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ailori
 */
@RestController
@RequestMapping(ACCESS_ROOT_MAP)
public class PaymentController {

    @Autowired
    private PaymentFacade2 paymentFacade;

    @ResponseBody
    @PostMapping(PAYMENT_URL)
    public ResponseEntity pay(@Valid @RequestBody BulkPaymentRequest request) {
        return ResponseEntity.ok().body(paymentFacade.createSchedule(request));
    }

    @ResponseBody
    @GetMapping(PAYMENT_COUNT_URL)
    public ResponseEntity countPay(@Valid @PathVariable String requestRef, @Valid @PathVariable String pageNumber) {
        return ResponseEntity.ok().body(paymentFacade.count(requestRef, pageNumber));
    }
}
