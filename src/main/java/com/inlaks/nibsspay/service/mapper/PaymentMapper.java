/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inlaks.nibsspay.service.mapper;

import com.inlaks.nibsspay.service.constants.ProjectConfig;
import com.inlaks.nibsspay.service.entity.PaymentTransactionRecord;
import com.inlaks.nibsspay.service.entity.TransactionDetailsRecord;
import com.inlaks.nibsspay.service.models.TransactionReferenceBean;
import com.inlaks.nibsspay.service.models.enums.NibssResposeCode;
import com.inlaks.nibsspay.service.models.enums.PaymentStatus;
import com.inlaks.nibsspay.service.models.inlaks.request.BulkPaymentRequest;
import com.inlaks.nibsspay.service.models.inlaks.request.PaymentRequest;
import com.inlaks.nibsspay.service.models.inlaks.response.TransactionStatusQueryResponse;
import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRecord;
import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRecords;
import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRequestCommand;
import com.inlaks.nibsspay.service.models.nibsspay.request.PaymentRequestProcess;
import com.inlaks.nibsspay.service.models.nibsspay.request.ProcessPaymentRequest;
import com.inlaks.nibsspay.service.models.nibsspay.response.PaymentStatusResponse;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.inlaks.responsecode.InlaksResponseCodeEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ailori
 */
@Slf4j
@Component
public class PaymentMapper extends ProjectConfig {

    @Autowired
    private TransactionReferenceBean transactionReferenceBean;

    /**
     * Converts a class to XML string
     *
     * @param <T>
     * @param t
     * @return
     */
    public <T> String convertObjectToXmlString(T t) {
        try {

            //Create JAXB Context and Create Marshaller
            Marshaller jaxbMarshaller = JAXBContext.newInstance(t.getClass()).createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(t, sw);

//            log.info(sw.toString());
            //Verify XML Content
            return sw.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * converts a String to a class object
     *
     * @param <T>
     * @param StringObject
     * @param t
     * @return
     */
    public <T extends Object> T convertStringToObject(String StringObject, Class<T> t) {

//        File xmlFile = new File(StringObject);
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(t.getClass());
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (T) jaxbUnmarshaller.unmarshal(IOUtils.toInputStream(StringObject, StandardCharsets.UTF_8));
        } catch (JAXBException e) {
//            e.printStackTrace();
            try {
                T newInstance = t.newInstance();
                Field f = t.getDeclaredField("content");
                f.setAccessible(true);
                f.set(newInstance, StringObject);
                return newInstance;
            } catch (InstantiationException | NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                log.info(ex.getMessage());
            }
        }
        return null;
    }

    public Function<BulkPaymentRequest, PaymentRequestCommand> mapRequestToPaymentSchedule = request -> {
        PaymentRequestCommand payment = new PaymentRequestCommand();
        payment.setDebitDescription(request.getDebitAccountName());
        payment.setScheduleId(transactionReferenceBean.getTransactionReference());
        payment.setClientId(getClientId());
        BeanUtils.copyProperties(request, payment);
        return payment;
    };

    public Function<PaymentRequestCommand, PaymentTransactionRecord> mapRequestToPaymentTransactionRecord = request -> {
        PaymentTransactionRecord record = new PaymentTransactionRecord();
        record.setBatchId(request.getScheduleId());
        record.setClientId(getClientId());
        return record;
    };

    public Function<PaymentRequest, TransactionDetailsRecord> mapRequestToTransactionDetails = request -> {
        TransactionDetailsRecord record = new TransactionDetailsRecord();
        BeanUtils.copyProperties(request, record);
        record.setAccountNumber(request.getBeneficiaryAccountNumber());
        record.setBankCode(request.getBeneficiaryBankCode());
        record.setBeneficiary(request.getBeneficiaryAccountName());
        record.setPaymentStatus(PaymentStatus.CREATED);
        record.setAmount(Integer.toString(request.getAmount()));
        return record;
    };

    public Function<List<TransactionDetailsRecord>, PaymentRequestProcess> mapRequestToPaymentRequestProcess = records -> {
        int pageNumber = 1;
        PaymentRequestProcess requestProcess = new PaymentRequestProcess();
        requestProcess.setPageNumber(pageNumber);
        requestProcess.setScheduleId(records.get(0).getPaymentTransactionRecord().getBatchId());
        requestProcess.setClientId(getClientId());

        List<PaymentRecord> paymentRecords = new ArrayList<>();

        records.stream().map((rec) -> {
            PaymentRecord record = new PaymentRecord();
            BeanUtils.copyProperties(rec, record);
            record.setAmount(rec.getAmount());
            return record;
        }).forEachOrdered((record) -> {
            paymentRecords.add(record);
            // check if page number greater than 100 then push to second page, use recursion for this process
        });

        requestProcess.setPaymentRecords(paymentRecords);

        return requestProcess;
    };

    public Function<List<TransactionDetailsRecord>, ProcessPaymentRequest> mapRequestToProcessPaymentRequest = record -> {
        ProcessPaymentRequest processPaymentRequest = new ProcessPaymentRequest();
        processPaymentRequest.setScheduleId(record.get(0).getPaymentTransactionRecord().getBatchId());
        processPaymentRequest.setClientId(getClientId());
        return processPaymentRequest;
    };

    public Function<PaymentStatusResponse, TransactionStatusQueryResponse> mapStatusResponseToTSQ = paymentStatusResponse -> {
        TransactionStatusQueryResponse transactionStatusQueryResponse = new TransactionStatusQueryResponse();
        transactionStatusQueryResponse.setScheduleId(paymentStatusResponse.getScheduleId());
        transactionStatusQueryResponse.setResponseCode(NibssResposeCode.getInlaksAgencyResponseMapFromCode(paymentStatusResponse.getStatus()).getResponseCode());
        transactionStatusQueryResponse.setResponseDescription(NibssResposeCode.getInlaksAgencyResponseMapFromCode(paymentStatusResponse.getStatus()).getResponseDescription());
        transactionStatusQueryResponse.setTransactionRef(paymentStatusResponse.getScheduleId());
        return transactionStatusQueryResponse;
    };

    public Function<PaymentTransactionRecord, TransactionStatusQueryResponse> mapTransactionRecordToStatusResponse = record -> {
        TransactionStatusQueryResponse response = new TransactionStatusQueryResponse();
        response.setScheduleId(record.getBatchId());
        response.setTransactionRef(record.getRequestId());
        response.setResponseCode(record.getPaymentStatus() == PaymentStatus.SUCCSSFUL ? InlaksResponseCodeEnum.RESPONSE_00.getResponseCode() : InlaksResponseCodeEnum.RESPONSE_20.getResponseCode());
        response.setResponseDescription(record.getPaymentStatus() == PaymentStatus.SUCCSSFUL ? InlaksResponseCodeEnum.RESPONSE_00.getResponseDescription() : InlaksResponseCodeEnum.RESPONSE_20.getResponseDescription());
        return response;
    };

}
